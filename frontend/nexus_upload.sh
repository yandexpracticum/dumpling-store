ARTIFACT_MD5_CHECKSUM=$(md5sum dumplings-store-${VERSION}.zip | awk '{print $1}')
ARTIFACT_SHA1_CHECKSUM=$(shasum -a 1 dumplings-store-${VERSION}.zip | awk '{ print $1 }')

curl --upload-file "dumplings-store-${VERSION}.zip" \
--header "X-Checksum-MD5:${ARTIFACT_MD5_CHECKSUM}" \
--header "X-Checksum-Sha1:${ARTIFACT_SHA1_CHECKSUM}" \
-u "${NEXUS_REPO_USER}:${NEXUS_REPO_PASSWORD}" \
-v ${NEXUS_REPO_URL_FRONTEND}dumplings-store/${VERSION}/dumplings-store-${VERSION}.zip
